#!/usr/bin/env sh

cd back-end
. venv/bin/activate
isort .
black .
deactivate

cd ../front-end
npm run format
