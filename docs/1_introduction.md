# Introduction to Contract Testing with Pact

For the purposes of this tutorial repo, I'll give a brief overview of what
contract testing is, why it's quickly gained popularity amongst top tech firms,
and clarify relevant terms I'll be using in the other documents.

For more detailed explanations, there are some really good videos, tutorials and
documentation surrounding where contract testing fits into the development and
testing of APIs. Here are a few you can look at separately:

- [Introduction to Contract Testing - YouTube](https://www.youtube.com/watch?v=IetyhDr48RI)
- [Introduction to Pact - Pact.io](https://docs.pact.io/)
- [How Pact Contract Testing Works - Pactflow.io](https://pactflow.io/how-pact-works/#slide-1)

## Establishing Terms

Where you have an integration point between two or more services, there will be
a means of communication between them - such as HTTP APIs or asyncronous
messaging services (such as RabbitMQ, Kafka, etc.)

Each of the services expects the communication to be performed in a certain way;
via a specific endpoint or queue, and predicated on sending or receiving
important bits of data during this interaction.

_For example, a frontend web app which requests data to fill a newsfeed from a_
_backend server. The server might expect this request via a certain HTTP_
_endpoint, and the UI expects the data to contain the fields it needs to render_
_the newsfeed correctly._

The service requesting the data is known as the **consumer**, and the service
supplying that data is known as the **provider**. Where you have expansive
applications with microservice architecture patterns, you may have services
which can be a consumer to some components or services, and a provider to others
at the same time.
The expected request/response communication is called an **interaction**, and
these interactions are established into **contracts, or pacts**.

## So What is Contract Testing?

Let's use our example from above. We've a frontend UI consumer, and a backend
server provider. The consumer needs to make requests to the provider in order
to obtain the data to populate its newsfeed.
This could involve one team/codebase, or multiple teams/codebases. But there's
an agreed endpoint that's going to provide this data - let's say `/news`.

How do we know that this interaction is going to work in production?

Well, one method would be to unit test the logic of each service independently.
We can verify the UI can render the newsfeed with a mocked backend, and verify
the backend can supply the data with a mocked request. These tests are the
fastest and easiest to run, after all.

[<img src="images/1a_unit_testing.png" width=25% height=25% target="_blank" />](images/1a_unit_testing.png)

Which is great - but how do you know your mock is accurate?
What if development of a new feature on the backend changes the endpoint to
`/newsfeed`, or changes an important field on the response?

The mocks can easily become out of date, and the tests written for the frontend
could still pass with a false positive... until the services are live and
suddenly there's a production issue.

Another method would be to use integration tests. With running the tests against
a real frontend and backend service, you have much greater accuracy on the
validity of interactions between the two.

[<img src="images/1b_integration_testing.PNG" width=25% height=25% target="_blank" />](images/1b_integration_testing.PNG)

However, these tests are slower and require more resources, especially as you
get into larger applications which have lots of services or integration points.

This is where contract testing comes in.

Contract testing is _"a technique for testing an integration point by checking_
_each application in isolation to ensure the messages it sends or receives_
_conform to a shared understanding that is documented in a 'contract'."_[^1]

Instead of needing to run both services, a contract test takes the expected
interaction (which would be mocked in our unit testing example) and ensures the
accuracy by enshrining it in a contract. This contract is then stored, and used
to test against both the consumer and the provider.

Provided that each service adheres to the specified contract, then you have the
confidence they can interact in production. This allows for a much quicker
testing, whilst retaining the confidence in future deployments.

## What is Pact?

Pact is a code-first tool which enables efficient and effective testing of
service interactions through contract testing. It has a number of really
valuable features designed specifically with contract testing in mind, and has
been adopted by companies such as [Atlassian, Gov.uk](https://pactflow.io/case-studies/)
and [Mastercard](https://developer.mastercard.com/blog/consumer-driven-contracts-to-the-rescue/).

Through Pact, we can test our consumer and provider separately against a Pact
mock server, but using the same contract - which is stored centrally in a Pact
broker and retained over time.

Whilst contract testing can be done without Pact, the hosted storage is one of
the main advantages of using a tool such as Pact when contract testing. This is
because Pact uses the stored contracts to form a matrix of compatibility across
various versions of each consumer and provider. So you can clearly see whether
a deployment will be compatible with dependent services _before_ it goes live.

With Pact, our contract testing process is split into a number of steps:

[<img src="images/1c_pact_contract_testing.PNG" width=40% height=40% target="_blank" />](images/1c_pact_contract_testing.PNG)

1. The consumer side test
2. Publishing the contract
3. The provider side verification
4. Publishing the verification

We'll cover the process for these in the tutorial documents in this repo.

## Our Demo Application

In this demo repository, our consumer is the frontend UI, and the provider is a
backend API.

The UI is rendering a simple audit log table, and requests data from the provider
at a `/log` endpoint. This data is retrieved from a PostgreSQL database, and
returned to the consumer.
Data can be added to the database by making a `POST` request to the `/log`
endpoint, but this isn't accessed by our consumer.

[<img src="images/1d_arch_diagram.png" width=35% height=30% target="_blank" />](images/1d_arch_diagram.png)

In the subsequent tutorials we'll be looking at how to contract test this
application within the same repository.

---

#### Footnotes

[^1]: Source: [Pact.io](https://docs.pact.io/#what-is-contract-testing)

