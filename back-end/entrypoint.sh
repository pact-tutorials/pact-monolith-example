#!/usr/bin/env sh

set -e

alembic upgrade heads
python3 -m api
