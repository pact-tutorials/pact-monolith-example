import uuid
from datetime import datetime

from sqlalchemy import Column, DateTime, String, create_engine
from sqlalchemy.dialects.postgresql import JSONB, UUID
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from .settings import Settings

engine = create_engine(Settings().db_url)
Session = sessionmaker(bind=engine)

Base = declarative_base()


class AuditLog(Base):
    """Represents a single audit log entry."""

    __tablename__ = "audit_log"

    id = Column(UUID, primary_key=True, default=lambda: str(uuid.uuid4()))
    timestamp = Column(DateTime, nullable=False, default=lambda: datetime.now())
    event_source = Column(String, nullable=False)
    event_type = Column(String, nullable=False)
    message = Column(String, nullable=False)
    additional_data = Column(JSONB, nullable=False, default=lambda: {})
