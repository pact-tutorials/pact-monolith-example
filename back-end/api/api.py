from email.generator import Generator
from typing import List

from fastapi import Depends, FastAPI, status
from fastapi.exceptions import HTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from pydantic import BaseModel
from sqlalchemy import select, delete
from sqlalchemy.orm import Session

from .models import AuditLog
from .models import Session as DBSession
from .schema import AuditLogRead, AuditLogWrite
from .settings import settings

app = FastAPI(
    debug=True,
    title="Audit Log API Service",
    docs_url="/docs",
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        "http://localhost:8081",
        "http://dorfl:8081",
        "http://localhost:5000",
        "http://dorfl:5000",
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

security = HTTPBasic()


def get_db() -> Generator:
    """Yield a db connection."""
    session = DBSession()
    yield DBSession()
    session.close()


def check_auth(credentials: HTTPBasicCredentials = Depends(security)) -> None:
    """Raise an error if the user is not allowed."""
    if (
        credentials.username != settings.api_user
        or credentials.password != settings.api_pass
    ):
        raise HTTPException(
            status.HTTP_403_FORBIDDEN, "You are not permitted to access that resource."
        )


@app.get("/log", response_model=List[AuditLogRead])
def get_audit_logs(session: Session = Depends(get_db)) -> List[AuditLogRead]:
    """Return all the audit logs in the system."""
    return [
        AuditLogRead.from_orm(log_entry)
        for log_entry in session.execute(
            select(AuditLog).order_by(AuditLog.timestamp)
        ).scalars()
    ]


@app.post("/log", status_code=status.HTTP_201_CREATED, response_model=AuditLogRead)
def add_audit_log(
    log_entry: AuditLogWrite,
    _: None = Depends(check_auth),
    session: Session = Depends(get_db),
) -> AuditLogRead:
    """Add a single audit log entry."""
    log_entry = AuditLog(**log_entry.dict())
    session.add(log_entry)
    session.commit()
    return AuditLogRead.from_orm(log_entry)


if settings.debug:
    import json
    import os

    class ProviderStateRequest(BaseModel):
        consumer: str
        state: str

    @app.post("/provider-states", status_code=status.HTTP_201_CREATED)
    def load_provider_state(
        provider_state: ProviderStateRequest, session: Session = Depends(get_db)
    ) -> None:
        """Load a provider state from a file."""
        state_path = "/app/tests/contract/provider/states/I have existing log entries.json"  # TODO: Look for .json files in this path rather than hard-coded file # noqa:E501

        if not os.path.exists(state_path):
            raise HTTPException(status.HTTP_404_NOT_FOUND, "State not found")

        with open(state_path, "r") as f:
            logs = json.loads(f.read())
        log_rows = [AuditLog(**log) for log in logs]

        session.execute(delete(AuditLog))
        session.add_all(log_rows)
        session.commit()
