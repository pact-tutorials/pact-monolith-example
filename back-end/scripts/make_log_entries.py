#!/usr/bin/env python3
import base64
import sys
from datetime import datetime, timedelta
from random import choice, randint
from time import sleep

import requests
from faker import Faker

auth = base64.b64encode(b"user:password").decode()
sources = [f"source_{i}" for i in range(5)]
types = [f"type_{i}" for i in range(5)]

count = 100
if len(sys.argv) > 1:
    count = int(sys.argv[-1])

fake = Faker()
for _ in range(count):
    entry = dict(
        timestamp=(datetime.now() - timedelta(days=randint(0, 365))).isoformat(),
        event_source=choice(sources),
        event_type=choice(types),
        message=fake.sentence(nb_words=randint(1, 20)),
    )

    result = requests.post(
        "http://localhost:5000/log",
        json=entry,
        headers={
            "Authorization": f"Basic {auth}",
        },
        timeout=10,
    )
    result.raise_for_status()

    print(result.json())
    sleep(0.1)
