import axios from 'axios'
import base64 from 'base-64'
import env from '../../env.js'

function getAuth () {
  const authStr = `${env.API_USER}:${env.API_PASSWORD}`
  return base64.encode(authStr)
}

async function _fetchLogs () {
  console.log(env)
  console.log(`Accesing ${env.API_URL}/log`)
  const result = await axios.get(
      `${env.API_URL}/log`, {
        headers: {
          Authorization: `Basic ${getAuth()}`
        }
      }
  )
  console.log(result)
  return result
}

async function fetchLogs () {
  const result = await _fetchLogs()
  return result.data
}

export { _fetchLogs, fetchLogs }
